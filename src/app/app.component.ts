import { Component } from '@angular/core';
import { SessionService } from './Shared/Services/sessionService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';

  constructor(private session: SessionService) {
    const token = this.session.get();
    if (token !== null) {
      console.log('Seems to be logged : ' + JSON.stringify(token));
      // Checking login
      if (this.session.ping() === false) {
        this.session.clear();
      }
    } else {
      console.log('Not logged');
    }
  }
}
