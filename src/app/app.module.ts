import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import {IndexPage} from './Pages/Index/index';
import {UsersPage} from './Pages/Users/users';
import {PostsPage} from './Pages/Posts/posts';
import {UserPage} from './Pages/User/user';

import {PostComponent} from './Pages/Posts/sub/Post/post';

import {PublishButton} from './Shared/Components/PublishButton/publishButton';
import {ListMessages} from './Shared/Components/ListMessages/listMessages';
import {AuthComponent} from './Shared/Components/AuthComponent/authComponent';

import {AppHeader} from './Templates/AppHeader/appHeader';
import {AppFooter} from './Templates/AppFooter/appFooter';

import {UsersService} from './Shared/Services/usersService';
import {MessagesService} from './Shared/Services/messagesService';
import {AuthService} from './Shared/Services/authService';
import {ApiService} from './Shared/Services/apiService';
import {BackEndMockService} from './Shared/Services/backEndMockService';
import {SessionService} from './Shared/Services/sessionService';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AppComponent, PublishButton,
    AppHeader, AppFooter,
    IndexPage, UsersPage,
    PostsPage, ListMessages,
    UserPage, PostComponent,
    AuthComponent,
  ],
  providers: [
    UsersService,
    MessagesService,
    AuthService,
    ApiService,
    BackEndMockService,
    SessionService,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
