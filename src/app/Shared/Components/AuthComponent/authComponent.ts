import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {ApiService} from '../../Services/apiService';
import {trigger, state, style, animate, transition} from '@angular/animations';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { SessionService } from '../../Services/sessionService';
import {TokenClass} from '../../TokenClass';
import {UserClass} from '../../UserClass';

@Component({
  selector: 'auth-comp',
  templateUrl: './authComponent.html',
  styleUrls: ['../../../Pages/Styles/main.scss', './authComponent.scss'],
  animations: [
    // Each unique animation requires its own trigger. The first argument of the trigger function is the name
    trigger('rotatedState', [
      state('default', style({ transform: 'rotate(0)' })),
      state('rotated', style({ transform: 'rotate(-180deg)' })),
      transition('rotated => default', animate('300ms ease-out')),
      transition('default => rotated', animate('300ms ease-in'))
    ])
  ]

})
export class AuthComponent implements OnInit {

  state: string = 'default';
  display: boolean;
  logs: FormGroup;
  loginState: boolean = true;
  signs: FormGroup;

  constructor(private router: Router, private route: ActivatedRoute,
              private api: ApiService, private session: SessionService) {
    this.display = false;
    this.logs = new FormGroup({
      username_input: new FormControl('', Validators.required),
      password_input: new FormControl('', Validators.required)
    });
    this.signs = new FormGroup({
      username_input: new FormControl('', Validators.required),
      password_input: new FormControl('', Validators.required)
    });
  }

  ngOnInit(): void {
  }

  toggleDisplay(): void {
    this.display = !this.display;
    this.state = (this.state === 'default' ? 'rotated' : 'default');
  }

  login(): boolean {
     return this.session.login(this.logs.value.username_input, this.logs.value.password_input);
  }

  logout(): boolean {
    this.session.logout();
    return true;
  }

  signin(): boolean {
    const user = this.api.createUser(this.signs.value.username_input, this.signs.value.password_input);
    return (user !== null);
  }

  switch(): void {
    this.loginState = !this.loginState;
  }

}
