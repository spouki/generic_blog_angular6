import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {UsersService} from '../../../Shared/Services/usersService';
import {MessagesService} from '../../../Shared/Services/messagesService';
import {UserClass as User} from '../../../Shared/UserClass';

@Component({
  selector:'list-messages',
  templateUrl: './listMessages.html',
  styleUrls: ['../../../Pages/Styles/main.scss','./listMessages.scss']
})
export class ListMessages implements OnInit {
  @Input() id:number;
  constructor(private router: Router, private route: ActivatedRoute,private usersService: UsersService, private messagesService: MessagesService) {
  }

  ngOnInit() {
  }

  get() {
  }

}
