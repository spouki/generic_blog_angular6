import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {UsersService} from '../../Services/usersService';
import {CommonModule} from '@angular/common';

@Component({
  selector: 'publish-button',
  templateUrl: './publishButton.html',
  styleUrls: ['./publishButton.scss'],
})
export class PublishButton {
  private opened: boolean;
  private post_content:string;
  private selectedUser:string;
  constructor(private router: Router, private route: ActivatedRoute,private users: UsersService) {
    this.opened = false;
    console.log(this.users.get());
  }

  ngOnInit() {}

  togglePopup() {
    this.opened = !this.opened;
  }

  post(value:string) {
    console.log('Sending : ' + value);
    console.log('Username : ' + this.selectedUser);
  }
}
