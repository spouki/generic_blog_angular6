export class MessageClass {
  id:number;
  title:string;
  content:string;
  date:number;
  author:number;
  receiver:number;

  // constructor() {}

  constructor(id:number,title:string, content:string, date:number, author:number, receiver:number) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.date = date;
    this.author = author;
    this.receiver = receiver;
  }

  getId() : number {
    return this.id;
  }
  getName() : string {
    return this.title;
  }
  getContent() : string {
    return this.content;
  }
  getDate() : number {
    return this.date;
  }
  getAuthor() : number {
    return this.author;
  }
  getReceiver() : number {
    return this.receiver;
  }
}
