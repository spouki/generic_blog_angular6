import { Injectable } from '@angular/core';
import {TokenClass} from '../TokenClass';
import {ApiService} from './apiService';
import {UserClass} from '../UserClass';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  token: TokenClass;

  constructor(private api: ApiService) {
    const session_token_saved = JSON.parse(window.localStorage.getItem('token'));
    if (session_token_saved === undefined) {
      // No session saved
      this.token = new TokenClass();
    } else {
      // Session saved
      this.token = new TokenClass(session_token_saved);
    }
  }

  get(): TokenClass|null {
    return this.token.isFilled() ? this.token : null;
  }

  getUser(): UserClass {
    if (this.token === null) {
      return null;
    } else {
      const user = this.api.getUserByToken(this.token.access_token);
      if (user === null) {
        console.log('NO USER for this token');
        this.token = null;
        this.logout();
        return null;
      }
      return user;
    }
  }

  updateStorage(): void {
    window.localStorage.setItem('token', JSON.stringify(this.token));
  }

  set(newToken: TokenClass): void {
    console.log('Setting new token as token : ' + JSON.stringify(newToken));
    this.token = newToken;
    this.updateStorage();
  }

  refresh(): TokenClass {
    if (this.token) {
      const token = this.api.refresh(this.token.getRefresh());
      this.token.clear();
      this.token = token;
      this.updateStorage();
      return token;
    }
    return null;
  }

  ping(): boolean {
    return this.api.pingBack();
  }

  clear(): void {
    if (this.token !== null) {
      this.token.clear();
    }
    window.localStorage.removeItem('token');
  }

  login(username: string, password: string): boolean {
    console.log('Username : ' + username + ' Password : ' + password);
    const token = this.api.login(username, password);
    console.log('TOKEN GOT : ' + JSON.stringify(token) + ' OF TYPE ' + typeof token);
    if (token !== null) {
      // Logged in
      this.set(token);
      return true;
    } else {
      // Not logged in
    }
    return false;
  }

  logout(): void {
    if (this.token) {
      if (this.api.logout(this.token.getAccess())) {
        console.log('Session token destroyed server side');
      }
    }
    this.clear();
  }

  isLogged(): boolean {
    if (this.token && this.token.isFilled()) {
      return true;
    }
    return false;
  }
}
