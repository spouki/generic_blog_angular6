import { ModuleWithProviders, Inject, Injectable, Optional } from '@angular/core';

import {BackEndMockService} from './backEndMockService';
import {TokenClass} from '../TokenClass';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private back: BackEndMockService) {
  }

  ping(): boolean {
    this.log('PING');
    return true;
  }

  log(message: string) {
    console.log(`AuthService: ${message}`);
  }

  login(username: string, password: string): TokenClass|null {
    const token = this.back.login(username, password);
    return token;
  }

  refresh(refresh_token: string): TokenClass {
    const token = this.back.refresh(refresh_token);
    return token;
  }


}
