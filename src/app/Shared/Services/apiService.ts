import { ModuleWithProviders, Inject, Injectable, Optional } from '@angular/core';
import {AuthService} from './authService';
import {MessagesService} from './messagesService';
import {UsersService} from './usersService';
import {TokenClass} from '../TokenClass';
import {UserClass as User, UserClass} from '../UserClass';
import {BackEndMockService} from './backEndMockService';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private auth: AuthService, private msg: MessagesService,
              private users: UsersService, private back: BackEndMockService) {}

  /*
    Auth API
  */
  login(username: string, password: string): TokenClass|null {
    const token = this.auth.login(username, password);
    return token;
  }
  refresh(refresh_token: string): TokenClass|null {
    const token = this.auth.refresh(refresh_token);
    return token;
  }
  logout(token: string): boolean {
    return this.back.logout(token);
  }

/*
    Messages API
*/
  getMessage(id: number): void {}
  getMessages(): void {}
  sendMessage(): void {}

/*
    Users API
*/
  getUser(id: number): UserClass {
    const user = this.back.getUser(id);
    return user;
  }
  getUserByToken(access_token: string): UserClass {
    const user = this.users.getByToken(access_token);
    return user;
  }
  getUsers(): User|Array<User>|null {
    const users = this.back.getUsers();
    return users;
  }
  createUser(username: string, password: string): UserClass {
    const user = this.back.createUser(username, password);
    return user;
  }

/*
    Misc API
*/
  pingBack(): boolean {
    return this.back.ping();
  }
}
