import { ModuleWithProviders, Inject, Injectable, Optional } from '@angular/core';
// import {AuthService} from './authService';
import {MessagesService} from './messagesService';
import {UsersService} from './usersService';
import {TokenClass} from '../TokenClass';
import {UserClass as User, UserClass} from '../UserClass';

@Injectable({
  providedIn: 'root'
})
export class BackEndMockService {

  constructor(private msg: MessagesService, private users: UsersService) {}

  ping(): boolean {
    return false;
  }

  login(username: string, password: string): TokenClass|null {
    const user = this.users.getByName(username);
    if (user === null) {
      return null;
    }
    if (user.getPassword() !== password) {
      return null;
    }
    user.generateToken();
    const tc = user.getToken();
    return tc;
  }

  refresh(refresh_token: string): TokenClass {
    const user = this.users.getByRefreshToken(refresh_token);
    if (user === null) { return null; }
    user.refreshToken();
    return user.getToken();
  }

  logout(token: string): boolean {
    const user = this.users.getByToken(token);
    if (user === null) { return false; }
    user.logout();
    return true;
  }

  createUser(username: string, password: string): UserClass {
    if (this.users.getByName(username)) { return null; }
    const user = this.users.add(username, password);
    return user;
  }

  getUsers(): User|Array<User>|null {
    const users = this.users.get();
    return users;
  }

  getUser(id: number): UserClass {
    return this.users.getById(id);
  }

}
