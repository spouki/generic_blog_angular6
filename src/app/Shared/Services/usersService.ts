import { ModuleWithProviders, Inject, Injectable, Optional } from '@angular/core';
import {UserClass, UserClass as User} from '../UserClass';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private users: Array<User>;

  constructor() {
    this.users = new Array() as Array<User>;
    console.log('Users Service created, need to Singleton it');
    this.users.push(new User(0, 'bob', 'pass'));
    // console.log(JSON.stringify(this.users[0]));
  }

  ping(): boolean {
    this.log('PING');
    return true;
  }

  log(message: string): void {
    console.log(`UsersService: ${message}`);
  }

  get(id?: number): User|Array<User>|null {
    if (id) {
      for (let idx in this.users) {
        if (this.users[idx].getId() === id) {
          return this.users[idx];
        }
      }
      return null;
    }
    return this.users;
  }

  getById(id: number): User {
    console.log('checking ? ' + id);
    for (let idx in this.users) {
      console.log('Checking user : ' + JSON.stringify(this.users[idx]) + ' with ' + id);
      if (this.users[idx].getId() === id) {
        return this.users[idx];
      }
    }
    return null;
  }

  getByToken(token: string): User|null {
    for (let idx in this.users) {
      if (this.users[idx].getAccessToken() === token) {
        return this.users[idx];
      }
    }
    return null;
  }

  getByRefreshToken(refresh_token: string): User {
    for (let idx in this.users) {
      if (this.users[idx].getRefreshToken() === refresh_token) {
        return this.users[idx];
      }
    }
    return null;
  }

  getByName(username: string): User|null {
    for (let idx in this.users) {
      if (this.users[idx].getName() === username) {
        return this.users[idx];
      }
    }
    return null;
  }

  add(name: string, password: string): UserClass {
    const id = this.users.length > 0 ? this.users.length : 0;
    const idx = this.users.push(new User(id, name, password));
    return this.users[idx - 1];
  }
}
