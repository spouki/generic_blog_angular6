import { ModuleWithProviders, Inject, Injectable, Optional } from '@angular/core';
import {MessageClass as Message} from '../MessageClass';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  private messages: Array<Message>;

  constructor() {
    this.messages = new Array() as Array<Message>;
    console.log('Messages Service created.');
    // this.messages.push(new Message(0,'Message title', 'Message content', +Date.now(), 0,1));
    // console.log(JSON.stringify(this.users[0]));
  }

  ping() {
    this.log('PING');
    return true;
  }

  log(message: string) {
    console.log(`MessagesService: ${message}`);
  }

  get(id?: number): Message|Array<Message>|null {
    if (id) {
      for (const idx in this.messages) {
        if (this.messages[idx].getId() === id) {
          return this.messages[idx];
        }
      }
      return null;
    }
    return this.messages;
  }

  add(title: string, content: string, date: number, author: number, receiver: number) {
    const id = this.messages.length > 0 ? this.messages.length : 0;
    console.log('ID +> ' + id);
    this.messages.push(new Message(id, title, content, date, author, receiver));
  }
}
