export class TokenClass {
  access_token: string;
  refresh_token: string;
  timestamp: number;
  expire: number;

  constructor(json?: object) {
    if (json) {
      // @ts-ignore
      this.access_token = json.access_token;
      // @ts-ignore
      this.refresh_token = json.refresh_token;
      // @ts-ignore
      this.timestamp = json.timestamp;
      // @ts-ignore
      this.expire = json.expire;
    }
  }

  generate(): void {
    this.access_token = this.guid();
    this.refresh_token = this.guid();
    this.timestamp = Math.floor(+Date.now() / 1000);
    this.expire = 3600;
  }
  refresh(): void {
    this.access_token = this.guid();
    this.refresh_token = this.guid();
    this.timestamp = Math.floor(+Date.now() / 1000);
    this.expire = 3600;
  }
  get(): TokenClass {
    return this;
  }
  getAccess(): string {
    return this.access_token;
  }
  getRefresh(): string {
    return this.refresh_token;
  }
  guid(): string {
    function s4(): string {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }
  clear(): void {
    this.access_token = null;
    this.refresh_token = null;
    this.timestamp = null;
    this.expire = null;
  }
  ping(): void { console.log('ping'); }
  isExpired(): boolean {
    if (this.timestamp + this.expire > Math.floor(+Date.now() / 1000)) {
      return true;
    }
    return false;
  }
  isFilled(): boolean {
    if (this.access_token && this.refresh_token && this.timestamp && this.expire) {
      return true;
    }
    return false;
  }
}
