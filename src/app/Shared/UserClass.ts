import {TokenClass} from './TokenClass';

export class UserClass {
  id: number;
  name: string;
  password: string;
  token: TokenClass;

  constructor(id: number, name: string, password: string) {
    this.id = id;
    this.name = name;
    this.password = password;
    this.token = new TokenClass();
  }

  getId(): number {
    return this.id;
  }
  getName(): string {
    return this.name;
  }
  getPassword(): string {
    return this.password;
  }
  generateToken(): void {
    if (this.token === null) {
      this.token = new TokenClass();
    }
    this.token.generate();
  }
  getToken(): TokenClass {
    return this.token;
  }
  getAccessToken(): string {
    return this.token.getAccess();
  }
  getRefreshToken(): string {
    return this.token.getRefresh();
  }
  refreshToken(): void {
    this.token.generate();
  }
  logout(): void {
    this.token.clear();
  }
}
