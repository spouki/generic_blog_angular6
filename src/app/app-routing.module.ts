import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IndexPage} from './Pages/Index/index';
import {UsersPage} from './Pages/Users/users';
import {PostsPage} from './Pages/Posts/posts';
import {UserPage} from './Pages/User/user';

const ROUTES: Routes = [
  { path: '', component: IndexPage},
  { path: 'users', component: UsersPage},
  { path: 'users/:id', component: UserPage},
  { path: 'posts', component: PostsPage},
  { path: 'posts/:id', component: PostsPage},

  // { path:'**', component:IndexPage}
  //   { path: '**', component: PageNotFoundPage },
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES,
    // {enableTracing: true}
    )],
  exports: [RouterModule],
  // declarations:[IndexPage,UsersPage,PostsPage]
})
export class AppRoutingModule { }
