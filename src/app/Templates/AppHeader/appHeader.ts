import {Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {SessionService} from '../../Shared/Services/sessionService';
import {ApiService} from '../../Shared/Services/apiService';
import {UserClass} from '../../Shared/UserClass';

@Component({
  selector: 'app-header',
  templateUrl: './appHeader.html',
  styleUrls: ['../../Pages/Styles/main.scss', './appHeader.scss']
})
export class AppHeader implements OnInit {

  // private isAuth: boolean = false;
  // private user: UserClass;

  constructor(private router: Router, private route: ActivatedRoute,
              private session: SessionService, private api: ApiService) {
    // const token = this.session.get();
    // if (token === null) {
    //   this.isAuth = false;
    // } else {
    //   if (token.timestamp + token.expire > Math.floor(+Date.now() / 1000)) {
    //     // Out of time, need to refresh
    //     if (this.session.refresh() !== null) {
    //       this.isAuth = true;
    //     }
    //   } else {
    //     this.isAuth = true;
    //   }
    // }
    // if (this.isAuth === true) {
    //   this.user = this.session.getUser();
    //   if (this.user === null) {
    //     // Error
    //     if (this.session.refresh()) {
    //       this.user = this.session.getUser();
    //     } else {
    //       this.user = null;
    //       this.isAuth = null;
    //       this.logout();
    //     }
    //   }
    // }
  }

  ngOnInit(): void {
    // this.route.url
    //   .subscribe(url => console.log('The URL changed to: ' + url));
  }

}
