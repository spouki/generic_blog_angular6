import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './appFooter.html',
  styleUrls: ['../../Pages/Styles/main.scss','./appFooter.scss']
})
export class AppFooter {
  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {}
}
