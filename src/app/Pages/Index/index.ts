import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './index.html',
  styleUrls: ['../../Pages/Styles/main.scss','./index.scss']
})
export class IndexPage implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  logIt(msg: string) {
    console.log(`#${msg}`);
  }

}
