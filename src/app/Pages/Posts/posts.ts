import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {CommonModule} from '@angular/common';
import {PostComponent} from './sub/Post/post';

var lorem = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam et feugiat enim. Aliquam erat volutpat. Vivamus posuere justo nec enim pellentesque, at porta ex dictum. Integer sit amet gravida mi. Nulla id lorem at elit commodo imperdiet. Proin dapibus quam fringilla dignissim varius. Nulla nec lorem gravida, placerat ante sit amet, elementum mi. Aliquam non ipsum urna. Proin mollis, est sed faucibus blandit, lacus ipsum molestie risus, quis blandit augue dolor ut metus. Nulla lobortis finibus fringilla. Curabitur volutpat nec urna vel fermentum. Vivamus vitae egestas lacus. In lacinia mauris et metus porta laoreet. Nam nec dolor luctus, maximus risus eget, sodales eros.
Cras auctor nibh ac tincidunt condimentum. Integer venenatis pellentesque tortor, at dictum eros vehicula a. Morbi euismod rutrum mi, at consectetur ligula efficitur quis. Sed auctor, nulla vitae aliquet condimentum, lectus enim fermentum tortor, a iaculis odio ipsum eu lorem. Integer mollis egestas nulla. Maecenas gravida porttitor erat, quis mollis velit elementum vel. Praesent pulvinar sit amet purus nec scelerisque.
Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Nullam vel nisi molestie, aliquet ex ac, gravida erat. Nulla aliquam vitae leo vel rutrum. Vestibulum fermentum sem elit, eu ullamcorper dolor hendrerit vitae. Suspendisse euismod pretium dictum. Proin id pellentesque ligula, nec tristique ligula. Aliquam eleifend pretium sollicitudin. Nulla facilisi. Aliquam erat volutpat. Sed pulvinar sagittis erat, nec posuere lectus ultrices sed. Vestibulum id odio finibus, ultricies lacus nec, gravida nibh. Ut in lorem nec tellus malesuada semper id eget ligula.
Mauris a nibh leo. Curabitur sodales lacus est, et maximus tellus varius sed. Cras consequat metus sed tellus eleifend porttitor. Integer bibendum dui quis enim viverra aliquam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed a velit non massa cursus tincidunt id eget odio. In elementum faucibus metus, tristique lobortis turpis sollicitudin non. Phasellus ut orci vel dolor congue vulputate eget sed diam. Proin sodales consectetur enim. Nam ipsum ex, feugiat vel feugiat nec, consectetur vel mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi quis urna id ligula porta lobortis.
Maecenas et auctor ligula. Phasellus ligula orci, maximus vel ligula id, iaculis elementum ligula. Sed arcu nulla, placerat vitae lacus nec, auctor pharetra eros. Mauris tortor urna, lobortis quis lectus id, lobortis bibendum enim. Cras sed eros id nisl lobortis tincidunt vitae non elit. Fusce eget laoreet eros, sit amet facilisis turpis. Morbi eu urna justo. Suspendisse in elit nunc. Donec mollis velit ligula, at accumsan nulla porta vitae. Nullam dictum, velit ut placerat mattis, leo nibh scelerisque leo, eu convallis ex eros at sem. Suspendisse non consectetur erat, vel accumsan ex. Nullam varius libero sodales faucibus aliquet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean malesuada purus in turpis efficitur, nec scelerisque orci vulputate.`

export class Post {
  title:string;
  content:string;
  date:number;
  id:number;
  constructor() {
    console.log('Post constructor');
    this.title = "Titre ";
    this.content = lorem;
    this.date = +Date.now();
    this.id = 0;
  }
  getTitle() {
    return this.title;
  }
  setTitle(newTitle) {
    this.title = newTitle;
  }
  getContent() {
    return this.content;
  }
  setContent(newContent) {
    this.content = newContent;
  }
  getDate() {
    return this.date;
  }
  setDate(newDate) {
    this.date = newDate;
  }
  getId() {
    return this.id;
  }
  setid(newId) {
    this.id = newId;
  }
  get() {
    return this;
  }
  set(newPost) {
    this.title = newPost.title;
    this.content = newPost.content;
    this.date = newPost.date;
    this.id = newPost.id;
  }
}

export class Posts {
  posts:Array<Post>;
  test:string;

  constructor() {
    this.test = "??";
    this.posts = new Array() as Array<Post>;
    for (let i = 0; i<2; i++) {
      this.posts.push(new Post());
    }
    console.log('Posts constructor');
  }
  get(id = null) {
    if (id) {
      for (let post of this.posts) {
        if (post.getId() === id) {
          return post;
        }
      }
    } else {
      return this.posts;
    }
    return null;
  }
}
// @NgModule({
//   imports:[
//     CommonModule
//   ]
// })
@Component({
  templateUrl: './posts.html',
  styleUrls: ['../../Pages/Styles/main.scss','./posts.scss'],
  // imports:[CommonModule]
  // declarations:[]
})
export class PostsPage implements OnInit {
  posts:Posts;

  constructor(private router: Router, private route: ActivatedRoute) {
    console.log('Posts');
    this.posts = new Posts();
    console.log('Posts');
  }

  ngOnInit() {
  }

  logIt(msg: string) {
    console.log(`#${msg}`);
  }

}
