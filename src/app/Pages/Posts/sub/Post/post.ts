// import { Router, ActivatedRoute } from '@angular/router';
import {CommonModule} from '@angular/common';
import { Component, HostBinding, OnInit,Input } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';


export class Post {
  title:string;
  content:string;
  date:number;
  id:number;
  constructor() {
    console.log('Post constructor');
    this.title = "Titre ";
    this.content = "Contenu ";
    // this.date = +Date.now()/1000;
    this.id = 0;
  }
  getTitle() {
    return this.title;
  }
  setTitle(newTitle) {
    this.title = newTitle;
  }
  getContent() {
    return this.content;
  }
  setContent(newContent) {
    this.content = newContent;
  }
  getDate() {
    return this.date;
  }
  setDate(newDate) {
    this.date = newDate;
  }
  getId() {
    return this.id;
  }
  setid(newId) {
    this.id = newId;
  }
  get() {
    return this;
  }
  set(newPost) {
    this.title = newPost.title;
    this.content = newPost.content;
    this.date = newPost.date;
    this.id = newPost.id;
  }
}
@Component({
  selector:'post-component',
  templateUrl: './post.html',
  styleUrls: ['./post.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        // width:'100%',
        margin:'auto',
        // height: 'auto',
        // paddingTop:'10px',
        // paddingBottom:'10px',
        textAlign:'center',
        opacity: 1,
        // backgroundColor: 'red',
        maxHeight:'800px',
        overflow:'auto',
      })),
      state('closed', style({
        // width:'100%%',
        margin:'auto',
        // paddingTop:'10px',
        // paddingBottom:'10px',
        textAlign:'center',
        minHeight:'100px',
        opacity: 0.5,
        // backgroundColor: 'green',
        // maxHeight: '200px',
        overflow:'auto',
      })),
      transition('open => closed', [
        animate('1000ms ease-out'),
      ]),
      transition('closed => open', [
        animate('1000ms ease-in'),
      ]),
    ]),
  ],
})

export class PostComponent implements OnInit{
  @Input() data:Post;
  @Input() ville:string;
  reduced:boolean;


  constructor() {
    this.reduced = true;
  }
  getTitle():string {
    return this.data.title;
  }
  setTitle(newTitle:string) {
    this.data.title = newTitle;
  }
  getContent():string {
    return this.data.content;
  }
  setContent(newContent:string) {
    this.data.content = newContent;
  }
  getDate():number {
    return this.data.date;
  }
  setDate(newDate:number) {
    this.data.date = newDate;
  }
  getId():number {
    return this.data.id;
  }
  setid(newId:number) {
    this.data.id = newId;
  }
  get():Post {
    return this.data;
  }
  set(newPost:Post) {
    this.data = newPost;
  }
  ngOnInit() {
  }
  logIt(msg: string) {
    console.log(`#${msg}`);
  }
  toggleReduced() {
    console.log('Toggling reduced');
    this.reduced = !this.reduced;
  }

}
