import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {UserClass as User} from '../../Shared/UserClass';
import {ApiService} from '../../Shared/Services/apiService';

@Component({
  templateUrl: './user.html',
  styleUrls: ['../../Pages/Styles/main.scss','./user.scss']
})
export class UserPage implements OnInit {
  id: number;
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService) {
    this.route.params.subscribe( params => {
      // console.log(params);
      this.id = parseInt(params.id, 10);
      // console.log(typeof this.id);
    } );
  }

  ngOnInit() {
  }

  get(): User {
    console.log('ID : ' + this.id);
    const user = this.api.getUser(this.id);
    console.log('User => ' + JSON.stringify(user));
    if (user === null) {
      this.router.navigate(['/users']);
    }
    return user;
  }

}
