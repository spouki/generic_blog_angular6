import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {UsersService} from '../../Shared/Services/usersService';
import {UserClass as User} from '../../Shared/UserClass';
import {ApiService} from '../../Shared/Services/apiService';

@Component({
  templateUrl: './users.html',
  styleUrls: ['../../Pages/Styles/main.scss', './users.scss']
})
export class UsersPage implements OnInit {
  private toggler: boolean;

  constructor(private router: Router, private route: ActivatedRoute,
              private usersService: UsersService, private api: ApiService) {
    this.toggler = false;
  }

  ngOnInit(): void {
  }

  logIt(msg: string): void {
    console.log(`#${msg}`);
  }

  toggleUserCreation(): void {
    this.toggler = !this.toggler;
  }

  createUser(name, pass): void {
    const user = this.api.createUser(name, pass);
  }
  getUsers(): User|Array<User> {
    return this.api.getUsers();
  }
}
